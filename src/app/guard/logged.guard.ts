import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, CanActivateChild, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthenticationService } from '../service/authentication.service';

@Injectable({
  providedIn: 'root'
})
export class LoggedGuard implements CanActivateChild {

  constructor(private auth:AuthenticationService, private router:Router){}

  canActivateChild(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

    if(this.auth.isLoggedIn()) {
      return true;
    }
    //On dit au router où nous rediriger si jamais on est pas loggé
    this.router.navigate(['contact']);
    return false;
    // return this.auth.isLoggedIn();
    // return this.auth.user;
  }
}
