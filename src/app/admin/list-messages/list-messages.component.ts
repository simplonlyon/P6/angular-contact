import { Component, OnInit } from '@angular/core';
import { MessageService } from 'src/app/service/message.service';
import { Message } from 'src/app/entity/message';

@Component({
  selector: 'app-list-messages',
  templateUrl: './list-messages.component.html',
  styleUrls: ['./list-messages.component.css']
})
export class ListMessagesComponent implements OnInit {
  page = 1
  pageSize = 5;
  messages:Message[] = [];
  constructor(private messaServ : MessageService) { }

  /**
   * Un getter pour récupérer côté template le morceau du tableau qu'on
   * souhaite afficher en fonction de la pagination (pour faire un truc vraiment
   * optimisé il aurait fallu que notre API REST prenne en compte la pagination
   * et qu'on puisse lui requêter seulement les données à afficher)
   */
  get pageMessages():Message[] {
    return this.messages.slice(this.pageSize*(this.page-1), this.pageSize*(this.page-1)+this.pageSize);
  }

  ngOnInit() {

    this.messaServ.findAll().subscribe(
      data => this.messages=data,
      error => console.error(error.message),
      () => console.info("Op success !")
    );
  }

  delete(id:number){
    this.messaServ.delete(id).subscribe(
      () => this.ngOnInit()
    );
  }

  

}
