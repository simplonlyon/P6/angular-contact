import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MessageService } from '../../service/message.service';
import { Message } from '../../entity/message';
import { switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.css']
})
export class MessageComponent implements OnInit {

  message: Message;

  constructor(private route: ActivatedRoute,
            private messaServ: MessageService, 
            private router:Router) {}

  ngOnInit() {
    //On récupère les paramètres de la route actuelle sous forme d'observable
    this.route.params.pipe(
      /* On rajoute un switchMap qui, quand l'observable de la route
      émettra des données, switchera sur l'observable du service message
      en utilisant le paramètre de la route récupéré */  
      switchMap(
        param => this.messaServ.find(param.id)
      )
    ).subscribe(
      //En cas de succès, on assigne le message au component
      message => this.message = message,
      //en cas d'erreur, on navigue sur la page not-found
      //(l'idéal serait de vérifier si on a un 404 pour envoyer vers not-found et envoyé vers une page "server error" si on a autre chose comme erreur)
      () => this.router.navigate(["/message-not-found"])
    )
  }

}
