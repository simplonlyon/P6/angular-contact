import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from "@angular/router";
import { FormsModule } from "@angular/forms";
import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";
import { NgbModule, NgbPaginationModule, NgbModalModule } from "@ng-bootstrap/ng-bootstrap";

import { AppComponent } from './app.component';
import { ContactComponent } from './contact/contact.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { MenuComponent } from './menu/menu.component';
import { LoginComponent } from './login/login.component';
import { AuthorizationInterceptor } from './service/interceptor/authorization.service';
import { ProtectedComponent } from './protected/protected.component';
import { TestGuard } from './guard/test.guard';
import { ListMessagesComponent } from './admin/list-messages/list-messages.component';
import { MessageComponent } from './admin/message/message.component';
import { LoggedGuard } from './guard/logged.guard';


const routes:Routes = [
  {path:"contact", component:ContactComponent},
  //Si on veut que la route ne soit accessible que dans certaines conditions
  //on ajoute un canActivate qui attend un tableaur de classes Guard en argument.
  //Ces classes determineront si on a accès ou pas à la route
  {path:"protected", component:ProtectedComponent, canActivate:[TestGuard]},
  //Si on veut que les routes enfants d'une route ne soit accessible que
  //dans certaines conditions, alors il faudra utiliser la propriété canActivatechild
  //qui attend des classes Guard qui implémentent l'interface CanActivateChild
  //Si on met juste le canActivateChild, la route en elle même reste accessible mais pas
  //les enfants et inversement si on met juste le canActivate
  {path: "admin", canActivateChild:[LoggedGuard], children: [
    {path: "message", component:ListMessagesComponent},
    {path: "message/:id", component:MessageComponent},
  ]},
  {path:"", redirectTo:"/contact", pathMatch:"full"},
  {path:"**", component:NotFoundComponent}
];
@NgModule({
  declarations: [
    AppComponent,
    ContactComponent,
    NotFoundComponent,
    MenuComponent,
    LoginComponent,
    ProtectedComponent,
    ListMessagesComponent,
    MessageComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    RouterModule.forRoot(
      routes
    ),
    NgbModule,
    NgbModalModule,
    NgbPaginationModule,
    NgbModule.forRoot()
  ],
  providers: [
    //On ajoute notre intercepteur http dans les providers du app.module
    //ce qui fait que toutes les requêtes http de l'appli seront affectées par
    //celui ci.
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthorizationInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
