import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NotFoundComponent } from './not-found.component';

describe('NotFoundComponent', () => {
  let component: NotFoundComponent;
  let fixture: ComponentFixture<NotFoundComponent>;

  beforeEach(async(() => {
    /**
     * Le TestBed représente l'application angular de test, il devra
     * contenir la déclaration et les imports de tout ce dont dépend
     * le component qu'on test actuellement (et pas plus)
     */
    TestBed.configureTestingModule({
      declarations: [ NotFoundComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NotFoundComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should contains one p', () => {
    let p:HTMLParagraphElement = fixture.nativeElement.querySelector('p');
    expect(p).toBeTruthy();
  });
  //Faire un test qui vérifie si le paragraphe contient bien la
  //phrase dans la variable error du component
  it('should contains one p with error inside', () => {
    let p:HTMLParagraphElement = fixture.nativeElement.querySelector('p');
    //on vérifie si le paragraph contient bien la valeur de la variable error du component
    expect(p.textContent).toContain( component.error );
    //On change la valeur de la propriété du component
    component.error = 'bloup';
    //On déclenche la mise à jour du template selon les nouvelles valeurs du component
    fixture.detectChanges();
    //On vérifie si le template s'est bien mis à jour
    expect(p.textContent).toContain( component.error );

  })
});
