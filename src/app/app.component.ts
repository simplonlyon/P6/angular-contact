import { Component } from '@angular/core';
import { Link } from './menu/link';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  links:Link[]=[
    {label:"Contact", url:"/contact"},
    {label:"Protected", url:"/protected"}
  ];
}
