import { Component, OnInit, Input } from '@angular/core';
import { Link } from './link';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {
@Input()
  links:Link[] = [];

  constructor() { }

  ngOnInit() {
  }

}
