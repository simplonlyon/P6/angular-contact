import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Message } from '../entity/message';

@Injectable({
  providedIn: 'root'
})
export class MessageService {
  private url = 'http://localhost:8080/api/message/';

  constructor(private http:HttpClient) { }

  findAll():Observable<Message[]> {
    return this.http.get<Message[]>(this.url);
  }

  find(id:number): Observable<Message> {
    return this.http.get<Message>(this.url + id);
  }

  add(message:Message): Observable<Message> {
    return this.http.post<Message>(this.url, message);
  }

  update(message:Message): Observable<Message> {
    return this.http.put<Message>(this.url + message.id, message);
  }

  delete(id:number): Observable<any> {
    return this.http.delete(this.url + id);
  }

}
