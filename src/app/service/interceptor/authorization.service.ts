import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { AuthenticationService } from '../authentication.service';

@Injectable({
  providedIn: 'root'
})
export class AuthorizationInterceptor implements HttpInterceptor{

  constructor(private auth:AuthenticationService) { }

  intercept(req: HttpRequest<any>, 
            next: HttpHandler): Observable<any> {
    let token = localStorage.getItem('token');
    if(token) {
      //si on a le token, on clone la requête interceptée.
      let modifiedRequest = req.clone({
        //Dans la requête clonée, on rajoute les headers qu'on veut
        setHeaders: {
          //en l'occurrence, le Authorization Bearer avec token
          Authorization: 'Bearer '+token
        }
      });
      //puis on donne au httpClient la requête modifiée
      return next.handle(modifiedRequest)
      //On rajoute un pipe sur l'Observable qui surveille la requête
      .pipe(
        //Et on lui dit de catcher les erreurs de cette requête
        catchError((err, caught) => {
          //Si l'erreur a un status 401, ça veut dire que notre token
          //est invalide
          if(err.status === 401) {
            //On dit donc au service authentication de nous logout
            this.auth.logout();
          }
          //puis on fait suivre l'erreur
          return throwError(err);
        })
      );
    }
    //si ya pas de token, on repasse la requête sans y toucher
    return next.handle(req);
  }
  
}
