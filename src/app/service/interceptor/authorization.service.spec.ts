import { TestBed } from '@angular/core/testing';

import { AuthorizationInterceptor } from './authorization.service';

describe('AuthorizationService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AuthorizationInterceptor = TestBed.get(AuthorizationInterceptor);
    expect(service).toBeTruthy();
  });
});
