import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';
import { tap } from "rxjs/operators";
import * as jwtdecode from "jwt-decode";


@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  private url = 'http://localhost:8080/api/login_check';
  /**
   * Un BehaviorSubject est un Subject qui conserve sa dernière
   * valeur émise.
   * Un Subject est un Observable sur lequel on peut pousser de
   * nouvelles valeurs avec la méthode next()
   * On va ici s'en servir pour avoir l'état d'authentification de
   * notre user, on pousse un bouléen dedans là, mais on pourrait
   * aussi pousser un object user histoire d'avoir l'utilisateur.ice
   * authentifié.e sous la main. On l'initialise ici à false (non loggé)
   */
  user = new BehaviorSubject<any>(null);

  constructor(private http:HttpClient) {
    /**
     * On dit que si on a le token dans le localStorage, on pousse
     * le token décodé dans le BehaviorSubject
     */
    if(localStorage.getItem('token')) {
      this.user.next(
        jwtdecode(localStorage.getItem('token'))
        );
    }
  }

  login(username:string, password:string): Observable<any> {
    return this.http.post<any>(this.url, {
      username:username, 
      password:password
    }).pipe(
      /**
       * L'opérateur tap va exécuter ce qu'on lui dit de faire 
       * lorsqu'on se subscribe sur l'observable et que celui ci émet.
       */
      tap(response => {
        /**
         * Si la réponse du serveur contient le token, on met celui ci
         * dans le localStorage puis on pousse true dans le BehaviorSubject
         */
        if(response.token) {
          
          localStorage.setItem('token', response.token);
          /**
           * On pousse le token décodé dans le behaviorsubject histoire
           * de pouvoir avoir accès à quelques informations du user côté front
           */
          this.user.next(jwtdecode(response.token));
        }
      })
    );
  }
  /**
   * Le logout consiste juste à supprimer le token et à pousser false
   * dans le BehaviorSubject
   */
  logout() {
    localStorage.removeItem('token');
    this.user.next(null);
  }
  /**
   * Une alternative au BehaviorSubject serait de faire une méthode
   * isLoggedIn() qui renvoie un booléen selon si on a le token ou 
   * pas dans le localStorage (on pourrait aussi vérifier si le token
   * est expiré ou non)
   */
  isLoggedIn():boolean {
    if(localStorage.getItem('token')) {
      return true;
    }
    return false;
  }
}
