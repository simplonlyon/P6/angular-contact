import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../service/authentication.service';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  username = '';
  password = '';
  feedback = '';
  isLogged:Observable<any>;

  constructor(private authServ:AuthenticationService, 
    private router:Router,
    private modalService: NgbModal) { }

  ngOnInit() {
    this.isLogged = this.authServ.user;
  }
  
  /**
   * Méthode qui sert juste à ouvrir le modal
   */
  open(content) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'});
    
  }

  login() {
    this.authServ.login(this.username, this.password)
    .subscribe(() => {
      this.feedback = '';
      //quand on est loggé, on ferme toutes les modals
      this.modalService.dismissAll();
    },
    (error) => {
      if(error.status === 401) {
        this.feedback = "Credentials error";
      } else {
        this.feedback = "Server error";
      }
    });
  }

  logout() {
    this.authServ.logout();
    //On fait une redirection quand on se déconnecte
    this.router.navigate(['contact']);
  }

  /**
   * On refait une méthode logged qui return le résultat du isLoggedIn()
   * du auth service, et c'est de cette méthode qu'on se servira dans
   * les ngIf et dans les [hidden] etc.
   */
  logged() {
    return this.authServ.isLoggedIn();
  }

}
