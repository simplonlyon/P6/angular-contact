export interface Message {
  id?:number,
  sender:string,
  content:string,
  date?:any
}
