import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContactComponent } from './contact.component';
import { FormsModule } from '@angular/forms';
import { MessageService } from '../service/message.service';
import { of } from 'rxjs';

describe('ContactComponent', () => {
  let component: ContactComponent;
  let fixture: ComponentFixture<ContactComponent>;
  let spyService: jasmine.SpyObj<MessageService>;
  let inputContent:HTMLInputElement;
  let inputSender:HTMLInputElement;

  beforeEach(async(() => {
    spyService = jasmine.createSpyObj('MessageService', ['add']);
    TestBed.configureTestingModule({
      declarations: [ContactComponent],
      imports: [FormsModule],
      providers: [
        { provide: MessageService, useValue: spyService }
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContactComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    inputContent = 
      fixture.nativeElement.querySelector('#content');
    inputSender= 
      fixture.nativeElement.querySelector('#sender');
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should change feedback and add id on addMessage', () => {
    let date = new Date();
    spyService.add.and.returnValue(
      of({ id: 1, content: 'test', date: date, sender: 'test sender' })
    );
    component.message.date = date;
    
    inputContent.value = 'test';
    inputSender.value = 'test sender';

    inputContent.dispatchEvent(new Event('input'));
    inputSender.dispatchEvent(new Event('input'));

    fixture.detectChanges();
    
    expect(component.message.content).toBe('test');
    expect(component.message.sender).toBe('test sender');

    component.addMessage();

    expect(component.feedback).toBe('Message sent');
    expect(component.message.id).toBe(1);

  });
});
