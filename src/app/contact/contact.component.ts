import { Component, OnInit } from '@angular/core';
import { Message } from '../entity/message';
import { MessageService } from '../service/message.service';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {

  message:Message = {sender:"", content:"", date: new Date()};
  /** Message de feedback pour l'utilisateur.ice */
  feedback = "";
  constructor(private messageService:MessageService) { }

  ngOnInit() {
  }

  addMessage(){
    this.messageService.add(this.message)
    //première fonction pour le cas où ça marche
    .subscribe( (data) => { 
      this.feedback= "Message sent";
      this.message = data;
    }, 
    //deuxième fonction pour le cas où il y a une erreur
    () => this.feedback = "Message not sent");
  }

}
