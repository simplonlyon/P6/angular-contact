/**
 * Le describe est là pour indiquer quel fonction/classe on test
 */
describe('jasmine syntax', function() {
    
    /**
     * Le beforeEach est une méthode qui se déclenchera avant chaque
     * lancement de test contenu dans le describe, on s'en servira pour
     * préparer l'environnement de test (pour qu'il soit le même entre 
     * tous les tests)
     */
     beforeEach(() => {

     });
     /**
      * Les it représentent les tests en eux même, on doit indiquer
      * dedans ce qu'on test dedans (à quoi on s'attend)
      */
    it('should pass', function() {
        //On utilise les expect pour faire nos assertions. On donne
        //en argument la valeur qu'on test, et une méthode dans laquelle
        //on dit ce à quoi on s'attend comme valeur
        expect('bloup').toBeTruthy();
    });

    describe('testFunction', function() {
        //Faire un it() pour tester que quand on donne
        //'bloup' à testFunction, elle nous renvoit 'BLOUP'
        it('should transform to uppercase', function() {
            const param = 'bloup';
            const expectedResult = 'BLOUP';
            expect(testFunction(param)).toBe(expectedResult);
        });

        it('should throw error for invalid arg', function() {
            //Pour tester les erreurs, il faudra mettre la fonction
            //déclenchant l'erreur à l'intérieur d'une autre fonction
            expect(() => testFunction(10)).toThrowError();
        });
    });
});


function testFunction(param) {
    if(typeof(param) !== 'string') {
        throw new Error('Invalid parameter type. String expected.');
    }
    return param.toUpperCase();
}