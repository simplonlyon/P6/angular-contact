## Marche à suivre pour installer NgBootstrap dans un projet
(Il est déjà installé dans celui ci)
1. Installer ng-boostrap `npm install @ng-bootstrap/ng-bootstrap`
2. lancer `ng add @ng-bootstrap/schematics` pour qu'il récupère le css et l'ajoute au angular.json
3. Ajouter le NgbModule dans les imports du NgModule

II. Appli Angular init
1. cloner l'appli sur https://gitlab.com/simplonlyon/P6/angular-contact.git
2. Créer un component contact
3. Installer le router et créer une route "contact" qui pointe sur le component fait précédemment, faire également les routes habituelles (le notfound et la redirection)
4. Créer une interface Message qui ressemblera à l'entité message côté serveur
5. Créer un service MessageService et mettre le crud dedans (vous pouvez reprendre ce qu'on a fait dans le angular-exercice et find and replace)
6. Dans le ContactComponent et template, créer un formulaire pour ajouter un message (pas besoin de mettre d'input date, on prendra la date de maintenant) et faire qu'au submit, ça fasse appel au service pour faire le add

III. Authentication Front
1. Créer un service authentication dans le dossier service
2. Dans ce service, créer une méthode login(username:string,password:string) qui fera un post sur l'url /api/login_check
3. typé le post en any, et rajouter derrière un pipe et dans ce pipe, mettre la méthode tap()
4. Dans le tap, faire une fat arrow function qui aura un paramètre response, et dire que si response.token, alors on stock ça dans le localStorage.setItem('token', ...)
5. Faire une méthode logout() qui fera juste un localStorage.removeItem('token')
6. Créer un component login. Dans le ts de ce component, créer une propriété username et une propriété password (toute deux en string)
7. Toujours dans le ts du login component, rajouter une méthode login() qui fera appel à la méthode login du AuthenticationService en lui donnant this.username et this.password
8. Créer également une méthode logout qui fera appel au logout
9. Dans le template, créer un formulaire de login et lui dire de déclencher la méthode login du component
10. Créer également un bouton logout qui déclenche le logout
11. Charger le login component dans le app component

IV. Administration / Guard
1. Créer 2 components : admin/list-messages et admin/message
2. Créer une nouvelle route avec comme path "admin" et pas de component
3. à cette route, rajouter une propriété children avec un tableau comme valeur
4. Dans ce tableau, créer une route avec comme path "message" et comme component ListMessageComponent et une autre route avec comme path "message/:id" et comme component MessageComponent
5. Dans le login component, faire que quand on est loggé, il affiche un lien vers "/admin/message"
6. Créer une nouvelle guard LoggedGuard et dire dans le can activate qu'elle ne s'activera que si on est connecté.e (pour ce faire, il va falloir injecter le AuthenticationService dans le constructeur de la guard et utiliser une des manière disponible pour savoir si on est loggé.e ou non)
Faire que la classe implémente l'interface CanActivateChild au lieu de CanActivate et rajouter un Child au bout du nom de la méthode canActivate
7. Donner la guard au canActivateChild de votre route "admin"
8. Dans le component ListMessage, injecter le MessageService et faire un findAll pour choper les message et les mettre dans une propriété
9. Créer une méthode delete(message) qui fera un .delete() et mettra à jour la liste dans le subscribe
10. Dans le template, faire un ngFor pour afficher la liste des message avec juste le sender et la date et un bouton delete
11. Faire qu'au click sur le delete, ça déclenche la méthode delete du component

V. Suite et fin
(Aidez vous de l'exemple à cet endroit pour les différentes étapes : https://angular.io/guide/router#route-parameters-in-the-activatedroute-service )
1. Dans le template du ListMessage, faire que le sender de chaque message soit à l'intérieur d'un lien et que celui ci pointe sur la page /message/id-du-message, utilisez pour cela un  [routerLink] 
2. Dans le MessageComponent ts, injecter le message service et le ActivatedRoute dans le constructeur et créer une propriété message:Message
3. Dans le ngOnInit faire un pipe sur la propriété paramMap du ActivatedRoute, dans ce pipe, mettre un opérateur switchMap. Dans la fonction du switchMap, lui créer un paramètre "params" et faire un return du findOne du service en lui donnant comme argument params.get('id')
4. Faire un subscribe derrière le pipe et faire qu'en cas de succès, on assigne la réponse à la propriété message et qu'en cas d'erreur on fasse une redirection vers la page NotFound
5. Dans le template du MessageComponent, faire l'affichage du message.